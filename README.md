# `CutterFilter`: créer des sous-jeux de données avec des regex

## Présentation

`CutterFilter` est un CLI qui permet de filtrer un jeu de données en `csv` pour ne garder
que les entrées qui correspondent à une expression régulière fournie par l'utilisateurice
pour une colonne donnée. Il produit en sortie un fichier `csv`.

***Attention: la structure des jeux de données doit correspondre à celle présente dans les
fichiers [icono.csv](./in/icono.csv) et [carto.csv](./in/carto.csv), et décrite à la fin de
ce README.***

## Installation

```bash
git clone https://gitlab.inha.fr/snr/rich.data/cutterfilter.git  # clone repo
cd cutterfilter                                                  # move to folder
python3 -m venv env                                              # create env
source env/bin/activate                                          # source env
pip install -r requirements.txt                                  # install dependencies
```

## Usage

```bash
python cutterfilter.py "<REGEX>" -d "<icono|carto>" -c "<COLUMN NAME>" -p?
python cutterfilter.py "galerie\scolbert" -d "icono" -c "named_entity" -p
```

`CutterFilter` prend un argument et deux options:
- **`argument`** : la regex utilisée pour faire le filtre
  - *`<REGEX>` dans l'exemple ci-dessus*
- **`-d` `--dataset`** : le jeu de données à traiter, soit `icono`, soit `carto`
  - *`"<icono|carto>"`* dans l'exemple ci-dessus
- **`-c` `--column`** : le nom de la colonne sur laquelle faire le filtre (voir 
  ci-dessous pour les valeurs possibles)
  - *`-c "<COLUMN NAME>"` dans l'exemple ci-dessus*
- **`-p` `--partial`** : un *flag* indiquant de ne garder que quelques colonnes 
  dans le fichier de sortie (les colonnes qui servent à créer un cartel). 
  - *`-p?`* dans l'exemple au dessus.

Quelques remarques:
- le *matching* fait avec la regex est **insensible à la casse**
- les colonnes sont renommées dans le fichier en sortie

## VALEURS POSSIBLES POUR `-c` `--column`

Si `--dataset=="icono"`:

| institution | id_ | url_iiif | url_source | title | author_1 | author_2 | publisher | date_source | date_corr | technique | inventory_number | corpus | description | inscription | comment | theme | named_entity | id_location | internal_note | produced_richelieu | represents_richelieu | 
| ----------- | --- | -------- | ---------- | ----- | -------- | -------- | --------- | ----------- | --------- | --------- | ---------------- | ------ | ----------- | ----------- | ------- | ----- | ------------ | ----------- | ------------- | ------------------ | -------------------- |

Si `--dataset=="carto"`:
| institution | filename | description | address | id_location | corpus | url_source | date_source | internal_note | url_image | 
| ----------- | -------- | ----------- | ------- | ----------- | ------ | ---------- | ----------- | ------------- | --------- |

## Licence et crédits

Outil développé par Paul Kervegan pour le projet Richelieu sous licence [GNU GPL 3.0](./LICENSE).




