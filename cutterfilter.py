import pandas as pd
import click
import os
import re

# *************************** CONSTANTS *************************** #
IN = os.path.join(os.path.abspath(""), "in")
OUT = os.path.join(os.path.abspath(""), "out")
HEADERS_ICONO = [
    "institution"
    , "id_"
    , "url_iiif"
    , "url_source"
    , "title"
    , "author_1"
    , "author_2"
    , "publisher"
    , "date_source"
    , "date_corr"
    , "technique"
    , "inventory_number"
    , "corpus"
    , "description"
    , "inscription"
    , "comment"
    , "theme"
    , "named_entity"
    , "id_location"
    , "internal_note"
    , "produced_richelieu"
    , "represents_richelieu" 
]

HEADERS_CARTO = [
    "institution"
    , "filename"
    , "description"
    , "address"
    , "id_location"
    , "corpus"
    , "url_source"
    , "date_source"
    , "internal_note"
    , "url_image"
]


# *************************** PROCESSING PIPELINE *************************** #
class CutterFilter():
    """
    object holding our df and all functions
    """
    ds = None      # `icono` `carto`
    df = None      # the df
    col = None     # column to filter on
    par = None     # return a partial df, with not all columns
    fil = None     # term to filter by
    islist = None  # self.column contains lists
    newcol = None  # duplicate of `self.col` on which modifications are done, to preserve the original `col`
    
    def __init__(self, fil, ds, col, par):
        self.ds = ds
        self.col = col
        self.par = par
        self.fil = fil
        self.newcol = f"_{col}"
        self.hdr = HEADERS_ICONO if self.ds == "icono" else HEADERS_CARTO
        fpath = os.path.join(IN, f"{self.ds}.csv")
        self.df = pd.read_csv(
            fpath
            , sep="\t", quotechar='"', header=0, skip_blank_lines=True
            , names=self.hdr, index_col=False
        ).dropna(subset=self.col, axis=0)  # remove all rows with NAN vals for `column`, since they can't be used to filter
        return
    
    def clean(self):
        """
        clean the targeted column to prepare for processing.
        the original column is kept as is, and the clean column
        is saved to the `self[newcol]` column
        """
        
        # if it's a list (has `|` field seperator), retype it to list 
        # before cleaning (to take account for typos, we consider that 
        # the `|` must be used at least 10 times)
        if self.df.loc[ self.df[self.col].str.contains("|") ].shape[0] > 10:
            self.df[self.newcol] = (self.df[self.col].str.replace("\s*\|\s*", "|", regex=True)
                                                     .str.lower()
                                                     .str.strip()
                                                     .str.split("|"))
            self.islist = True
        # else, just do the cleaning
        else:
            self.df[self.newcol] = (self.df[self.col].str.replace("\s+", " ", regex=True)
                                                     .str.lower()
                                                     .str.strip())
        return self

    def filter(self):
        """
        filter the dataframe to keep only rows where `self.newcol` matches `self.fil`
        """
        self.df[self.newcol] = self.df[self.newcol].apply(
            lambda x: any([ re.search(self.fil, n) for n in x ])  # True where `self.newcol` does match `self.fil`
        )
        self.df = self.df.loc[ self.df[self.newcol] ]  # drop all rows that don't match
        return self
    
    def partial(self):
        """
        remove necessary columns from the df
        """
        self.df = self.df.drop(labels=[ self.newcol ], axis=1)
        if self.par and self.ds == "icono":
            self.df = self.df.drop(columns=[ 
                "id_"
                , "corpus"
                , "description"
                , "inscription"
                , "comment"
                , "theme"
                , "named_entity"
                , "id_location"
                , "internal_note"
                , "produced_richelieu"
                , "represents_richelieu" 
            ])
        elif self.par and self.ds == "carto":
            self.df = self.df.drop(columns=[
                "filename"
                , "id_location"
                , "internal_note"
            ])
        return self 
    
    def write(self):
        """
        write the df to file
        """
        fpath = os.path.join(OUT, f"{self.ds}.csv")
        self.df.to_csv(fpath
                       , sep="\t", quotechar='"'
                       , index=False if self.par else True
                       , index_label="index", encoding="utf-8"
        )
        return self
    
    def pipeline(self):
        """
        processing pipeline
        """
        self.clean().filter().partial().write()
        click.echo(f"\n{self.df.shape[0]} rows matched for `{self.fil}`\n"
                   + f"output written to `{os.path.join(OUT, 'icono.csv')}`\n")
        return


# *************************** CLI LOGIC *************************** #
@click.command()
@click.argument("filter")
@click.option("-d", "--dataset", type=click.Choice(["icono", "carto"]), required=True
              , help="dataset to process")
@click.option("-c", "--column", type=click.Choice( sorted(set(HEADERS_ICONO + HEADERS_CARTO)) )
              , required=True, help="a column name")
@click.option("-p", "--partial", required=False,is_flag=True
              , help="flag to keep only a subset of columns")
def main(filter, dataset, column, partial):
    """
    filter the iconography dataset: keep only
    rows matching the FILTER argument (a regex string)
    on a COLUMN provided with the `-c` option.
    regex matching is CASE INSENSITIVE.
    
    EXAMPLE: `python cutterfilter.py "galerie\scolbert" -c named_entity -p`
    """
    if not os.path.isdir(IN):
        os.makedirs(IN)
    if not os.path.isdir(OUT):
        os.makedirs(OUT)
    
    CutterFilter(filter, dataset, column, partial).pipeline()
    return 
    

if __name__ == "__main__":
    main()



